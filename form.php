<?php
require_once("include/header.php");
include_once("include/dbConexion.php");

if (isset($_POST['agregar']) && !empty($_POST['agregar'])) {
    $errors = [];
    $datos = $_POST;
    unset($datos['agregar']);

    foreach ($datos as $key => $dato) {
        if (empty($dato)) {
            $errors[$key] = "El campo $key es obligatorio.";
        }
    }

    if (empty($errors)) {
        # code...
    }
}

# firstName, Lastname, JobTitle , id - 1143
$sql = "select employeeNumber, concat(firstName, ' ', lastName) AS nombre, jobTitle FROM employees AS e WHERE employeeNumber <= 1143";
$jefes = getDatos($sql);

# ciudades
$sql = "select distinct o.country from offices o order by country";
$ciudades = getDatos($sql);

# oficinas
$sql="select officeCode, city, country from offices order by country";
$oficinas = getDatos($sql);
?>
  <body>
    <main class="container-fluid">
        <a href='index.php' class='btn btn-success mt-2'>Ir a usuarios</a>
        <h1>Registro nuevo empleado</h1>
        <form id="frm_employee" method="POST" action="./form.php">
            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="firstName">Nombre(s)</label>
                    <input type="text" class="form-control <?= (isset($errors['firstName'])) ? 'is-invalid' : "" ?>" name="firstName" id="firstName" placeholder="Nombre(s)">
                    <div class="invalid-feedback">
                        <?= (isset($errors['firstName'])) ? $errors['firstName'] : "" ?>
                    </div>
                </div>
                <div class="form-group col-md-5">
                    <label for="lastName">Apellido(s)</label>
                    <input type="text" class="form-control <?= (isset($errors['lastName'])) ? 'is-invalid' : "" ?>" name="lastName" id="lastName" placeholder="Apellido(s)">
                    <div class="invalid-feedback">
                        <?= (isset($errors['lastName'])) ? $errors['lastName'] : "" ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="email">Correo</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="correo@classicmodelcars.com">
                    <div class="invalid-feedback">
                        <?= (isset($errors['email'])) ? $errors['email'] : "" ?>
                    </div>
                </div>
                <div class="form-group col-md-5">
                <label for="extension">Extension</label>
                    <input type="text" class="form-control" name="extension" id="extension" placeholder="x0000" maxlength="10">
                    <div class="invalid-feedback">
                        <?= (isset($errors['extension'])) ? $errors['extension'] : "" ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
            <div class="form-group col-md-5">
                    <label for="jobTitle">Cargo</label>
                    <input type="text" class="form-control" name="jobTitle" id="jobTitle" placeholder="Cargo">
                    <div class="invalid-feedback">
                        <?= (isset($errors['jobTitle'])) ? $errors['jobTitle'] : "" ?>
                    </div>
                </div>
            <div class="form-group col-md-5">
                    <label for="reportsTo">Jefe </label>
                    <select name="reportsTo" id="reportsTo" class="form-control">
                        <option value="">-- N/A --</option>
                        <?php foreach($jefes as $jefe) {
                            echo "<option value='{$jefe['employeeNumber']}'>{$jefe['nombre']} - {$jefe['jobTitle']}</option>";
                        } ?>
                    </select>
                    <div class="invalid-feedback">
                        <?= (isset($errors['reportsTo'])) ? $errors['reportsTo'] : "" ?>
                    </div>
                </div>
            </div>
            <div class="form-row">
            <div class="form-group col-md-5">
                    <label for="city">Ciudad</label>
                    <select name="city" id="city" class="form-control">
                        <option value="">-- Todas --</option>
                        <?php foreach($ciudades as $ciudad){
                            echo "<option value='{$ciudad['country']}'>{$ciudad['country']} </option>";
                        };
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-5">
                    <label for="officeCode"><i id="officeCodeLoading" class="fas fa-spinner fa-spin d-none">&nbsp;</i>Oficina</label>
                    <select name="officeCode" id="officeCode" class="form-control">
                        <option value="">-- Selecciona una opci&oacute;n --</option>
                        <?php foreach($oficinas as $oficina){ ?>
                            <option value="<?= $oficina['officeCode']?>"><?= $oficina['city'] . ' (' . $oficina['country'] . ')';?></option>
                        <?php }?>
                    </select>
                    <div class="invalid-feedback">
                        <?= (isset($errors['officeCode'])) ? $errors['officeCode'] : "" ?>
                    </div>
                </div>
            </div>
            <button name="agregar" value="agregar" type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </main>
    <script src="assets/js/addEmpleados.js"></script>
<?php
require_once ("include/footer.php");
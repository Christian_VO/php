<?php
function conexion() {
    $params = parse_ini_file("db.conf");
    $dbc = mysqli_connect($params['host'], $params['user'], $params['clave'], $params['dbnombre']);
    if ($dbc) {
        mysqli_set_charset($dbc, $params['charset']);
    }
    return $dbc;
}

function getDatos($query) {
    $dbc = conexion();
    $datos = [];
    if ($dbc) {
        $result = mysqli_query($dbc, $query);
        if ($result) {
            $numRows = mysqli_affected_rows($dbc);
            if ($numRows) {
                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    $datos[] = $row;
                }
            }
        }
    }
    return $datos;
}

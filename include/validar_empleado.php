<?php
function validar_empleado($datos){
    $errors = [];
    $obligatorios = ['firstNames', 'lastName', 'email'];

    foreach ($datos as $key => $dato) {
        if (array_key_exists($key, $obligatorios) && empty($dato)) {
            $errors[$key] = "El campo $key es obligatorio.";
        }
    }
    return $errors;
}
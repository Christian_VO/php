<?php 
    include_once("include/header.php");
    include_once("include/dbConexion.php");

    $sql = 'SELECT * 
            FROM employees E
            INNER JOIN offices O
            ON E.officeCode = O.officeCode
            ORDER BY E.firstName DESC';

    $empleados = getDatos($sql);

    foreach ($empleados as $emp){
        $htmlCard = "<div class='col pt-3'>
                        <div class='card'>
                            <div class='card-header'>
                                {$emp['firstName']} {$emp['lastName']}
                            </div>
                            <div class='card-body'>
                                <h5 class='card-title'>{$emp['jobTitle']}</h5>
                                <p class='card-text'>{$emp['email']}</p>
                                <p class='card-text'>Extensión: {$emp['extension']}</p>
                                <p class='card-text'>País: {$emp['country']}</p>
                                <p class='card-text'>Ciudad: {$emp['city']}</p>
                                <p class='card-text'>Tel.: {$emp['phone']}</p>
                                <a href='#' class='btn btn-primary'>Detalles</a>
                            </div>
                        </div>
                    </div>";

        $empleadosInfo = empty($empleadosInfo) ? $htmlCard :$htmlCard.$empleadosInfo;
    }

?>

<main class="container-fluid">
    <a href='form.php' class='btn btn-success mt-2'>Ir al formulario</a>
    <h1>Usuarios</h1>
    <div class="row row-cols-4">
        <?= $empleadosInfo ?>
    </div>
</main>

<?php
include ("include/footer.php");
?>



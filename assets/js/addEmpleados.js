document.addEventListener('DOMContentLoaded', function() {
    $(document).ready(function() {
        $('#city').on('change', function() {
            $.ajax({
                url: 'ajax_getOficinas.php',
                method: 'POST',
                dataType: 'html',
                data: {
                    city: $(this).val()
                },
                success: (data, textStatus) => {
                    $('#officeCode').html(data);
                },
            });
        });
    });
});